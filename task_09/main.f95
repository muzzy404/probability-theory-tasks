program main
  implicit none
  integer re_i

  real(8), parameter :: a = 1.2
  real(8), parameter :: b = 2.0
  real(8), parameter :: Pi = 3.1415927

  integer, parameter :: REALISATIONS = 8000000

  real(8) :: angle, X, X_sum, P

  integer :: r

  X_sum = 0.0
  do r = 1, REALISATIONS

    call random_number(angle)
    angle = angle * (Pi/2)

    X = b * cos(Pi/2 - angle) + a * cos(angle)
    X_sum = X_sum + X
  end do

  P = X_sum / REALISATIONS

  write(*,"(F12.10)") P

  re_i = system("pause")
end
