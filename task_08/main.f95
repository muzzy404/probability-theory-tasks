program main
  implicit none
  integer re_i

  integer, parameter :: n = 23
  integer, parameter :: k = 18
  integer, parameter :: R = 5000000

  real :: L = 10.0
  real :: X, P

  real, dimension(n) :: pigeons
  integer :: i, realisation, num
  real :: counter

  counter = 0.0
  do realisation = 1, R
    call random_number(pigeons)
    do i = 1, n
      pigeons(i) = pigeons(i) * L
      !write(*,*) pigeons(i)
    end do

    call random_number(X)
    X = X * L

    num = 0
    do i = 1, n
      if (pigeons(i) < X) then
        num = num + 1
      end if
    end do

    if (k == num) then
      counter = counter + 1
    end if

    do i = 1, n
      pigeons(i) = 0
    end do

  end do

  P = counter / R
  write(*,"(F12.10)") P

  re_i = system("pause")
end
