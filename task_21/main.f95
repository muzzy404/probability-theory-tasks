 ! wolfram: (sum from 1 to 96 1/n)*96

 program main
  implicit none

  integer, parameter :: R = 1000000
  integer, parameter :: N = 50

  integer, dimension(N) :: icecream

  integer :: i, j, M, stop_sim, type_icecream, summa_M
  real(8) :: type_icecream_r

  summa_M = 0
  do i = 1, R

    do j = 1, N
      icecream(j) = 0
    end do

    stop_sim = 0
    M = 0
    do while (stop_sim < 1)
      M = M + 1

      call random_number(type_icecream_r)
      type_icecream_r = type_icecream_r * N
      type_icecream = aint(type_icecream_r)

      icecream(type_icecream + 1) = 1

      !write(*,*) icecream
      !write(*,*) sum(icecream)

      if (sum(icecream) == N) then
        stop_sim = 1
        !write(*,*) 'stop'
      end if

      !write(*,*) M

    end do

    summa_M = summa_M + M
    !write(*,*) summa_M

    if (mod(i, 100000) == 0) then
      write(*,*) ((100.0 * i) / R), '%'
    end if

  end do


  write(*,*) summa_M/R

end
