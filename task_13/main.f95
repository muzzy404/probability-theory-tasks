function rand_exponential(mu) result(res)
  implicit none

  real(8), intent(in) :: mu
  real(8) :: rand_num, res

  call random_number(rand_num)

  res = (log(1 - rand_num)) / (-mu)
end function

program main
  implicit none

  real(8) :: rand_exponential
  real(8) :: mu_1, mu_2, rand_1, rand_2, T, P, P_1, P_2
  real(8) :: catch_1, catch_2

  integer, parameter :: REALISATIONS = 40000000
  integer :: i

  mu_1 = 0.136
  mu_2 = 0.140
  T = 4.0

  catch_1 = 0.0
  catch_2 = 0.0
  do i = 1, REALISATIONS

    rand_2 = rand_exponential(mu_2)
    rand_1 = rand_exponential(mu_1)

    ! count separate
    if(rand_1 < T) then
      catch_1 = catch_1 + 1.0
    end if
    if(rand_2 < T) then
      catch_2 = catch_2 + 1.0
    end if

  end do

  P_1 = (catch_1 / REALISATIONS)
  P_2 = (catch_2 / REALISATIONS)

  P = P_1 + P_2 - P_1*P_2

  write(*,'(A10,F12.10)') 'answer  = ', P

  write(*,'(A10,F12.10)') 'P_1     = ', P_1
  write(*,'(A10,F12.10)') 'P_2     = ', P_2

end
