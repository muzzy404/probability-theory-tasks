program main
  implicit none
  integer re_i

  real, parameter :: a = -0.74
  real, parameter :: b =  0.81
  real, parameter :: Pi = 3.1415927

  real :: X, Y, counter, P

  integer, parameter :: REALISATIONS = 5000000
  integer :: r

  counter = 0.0
  do r = 1, REALISATIONS

    call random_number(X)
    X = X * (2*Pi)
    Y = cos(X)

    if (Y >= a .and. Y <= b) then
      counter = counter + 1.0
    end if
  end do

  P = counter / REALISATIONS
  write(*,"(F12.10)") P

  re_i = system("pause")
end
