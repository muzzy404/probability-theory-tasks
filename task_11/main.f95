
program main
  implicit none

  integer :: i, j

  integer, parameter :: n = 24
  real(8), parameter :: p = 0.24
  real(8), parameter :: k = 0.89

  integer, parameter :: REALISATIONS = 8000000

  real(8) :: health_lvl
  real(8) :: hit

  real(8) :: sum_health, sum_health_sqr

  sum_health     = 0.0
  sum_health_sqr = 0.0

  do i = 1, REALISATIONS

    health_lvl = 1.0

    do j = 1, n
      call random_number(hit)

      if (hit <= p) then
        health_lvl = health_lvl * k
      end if

    end do

    sum_health     = sum_health     + health_lvl
    sum_health_sqr = sum_health_sqr + health_lvl**2

    if (mod(i, 1000000) == 0) then
      write(*,*) i

      write(*,*) 'M     = ', (sum_health / i)
      write(*,*) 'M_sqr = ', (sum_health_sqr / i)
    end if

  end do

  write(*,'(A9,F12.10)') 'answer = ', sqrt((sum_health_sqr / REALISATIONS) - &
                                           (sum_health     / REALISATIONS)**2)

end
